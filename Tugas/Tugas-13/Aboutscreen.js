import React, { Component } from 'react';

import {
StyleSheet,
Text,
View,
TextInput,
TouchableOpacity,
Image

} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
export default class Logo extends Component {
render(){
return(
    <View style={styles.judul}>
        <Text style = {styles.logo}>Tentang Saya</Text>
<View style={styles.container}>

<TouchableOpacity>
    <Icon style = {styles.icons} name ='user' size={100}></Icon>
    </TouchableOpacity>
    <Text style ={styles.nama}>Mukhlis Hanafi</Text>
    <Text style ={styles.profesi}>React Native Developer</Text>
    <Text style = {styles.porto}>Portofolio</Text>
    <Text>_________________________________________</Text>
    <View style = { styles.navBar}>
<TouchableOpacity style={styles.tabItem}>
<Icon name ='gitlab' style = {styles.gitlab} size = {50} alignItems = 'left'></Icon>
<Text style={styles.tabTitle}>@mukhlish</Text>
</TouchableOpacity>
<TouchableOpacity style={styles.tabItem}>
<Icon name ='github' style = {styles.gitlab} size = {50} alignItems = 'left'></Icon>
<Text style={styles.tabTitle}>@mukhlish</Text>
</TouchableOpacity>

</View>
</View>
</View>
)
}
};
const styles = StyleSheet.create({

container : {

flexGrow: 1,

justifyContent:'center',
alignItems : 'center'

},
tabTitle: {
    fontSize: 11,
    color: '#3c3c3c',
    paddingTop: 4
  },
tabItem: {
    justifyContent : 'center',
    alignItems: 'center',
    marginLeft : 50,
    marginRight : 50
},
navBar:{
    backgroundColor: 'white',
    height: 90,
    borderColor: '#E5E5E5',
    flexDirection: 'row',
    justifyContent: 'space-around'
},
icons:{
 paddingTop : 40,
 alignContent : 'center',
},
gitlab:{
 padding :10
},
judul :{
  fontSize : 30,
  color : 'blue',
  paddingVertical : 20,
  marginTop : 20,
  alignItems : 'center'
},
logo : 
    {
      fontSize : 30,
      alignItems : 'center',
      margin : 20,
      paddingTop : 40,
      color : 'blue'
    },
    porto:{
      fontSize : 15,
      paddingTop : 20,
      textAlign : 'left',

    },
nama:{
    color : 'blue',
    paddingTop : 40,
    fontSize : 30, 
},
profesi :
{
  color : 'cyan',
  fontSize : 15,
  paddingTop : 5
}
})
