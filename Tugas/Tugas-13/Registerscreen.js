import React, { Component } from 'react';

import {
StyleSheet,
Text,
View,
TextInput,
TouchableOpacity,
Image

} from 'react-native';
export default class Logo extends Component {
render(){
return(
<View style = {styles.logo}>
< Image source ={require('./Images/logo.png')} style = {styles.logo}/>

<View style={styles.container}>
<Text style={styles.judul}>Register</Text>
<TextInput style={styles.inputBox}

underlineColorAndroid='rgba(0,0,0,0)'

placeholder= "Username"

selectionColor= "black"

onSubmitEditing={()=> this.password.focus()}

/>
<TextInput style={styles.inputBox}

underlineColorAndroid='rgba(0,0,0,0)'

placeholder= "Email"

selectionColor= "black"

keyboardType="email-address"

onSubmitEditing={()=> this.password.focus()}

/>
<TextInput style={styles.inputBox}

underlineColorAndroid='rgba(0,0,0,0)'

placeholder="Password"

secureTextEntry={true}


ref={(input) => this.password = input}/>
<TextInput style={styles.inputBox}

underlineColorAndroid='rgba(0,0,0,0)'

placeholder="Ulangi Password"

secureTextEntry={true}


ref={(input) => this.password = input}/>
<TouchableOpacity style={styles.button}>

<Text style={styles.buttonText}>{this.props.type}Daftar</Text>

</TouchableOpacity>
<Text>atau</Text>
<TouchableOpacity style={styles.button1}>

<Text style={styles.buttonText}>{this.props.type}Masuk?</Text>

</TouchableOpacity>
</View>
</View>
)}
}
const styles = StyleSheet.create({

container : {

flexGrow: 1,

justifyContent:'center',

alignItems: 'center'

},
inputBox: {

width:300,
height : 50,
backgroundColor:'#EFEFEF',

borderRadius: 25,

paddingHorizontal:16,

fontSize:16,

color:'black',

marginVertical: 10,
marginTop : 5

},
judul :{
  fontSize : 30,
  color : 'blue',
  paddingVertical : 40,
  marginTop : 70,
},
button1: {

  width:100,
  
  backgroundColor:'cyan',
  
  borderRadius: 25,
  
  marginVertical: 10,
  
  paddingVertical: 10
  
  },
button: {

width:100,

backgroundColor:'blue',

borderRadius: 25,

marginVertical: 10,

paddingVertical: 13

},

buttonText: {
fontSize:15,
color:'#ffffff',
textAlign:'center'

},
logo : 
    {
      width : 290,
      height : 100,
      alignItems : 'center',
      margin : 20,
    }

});
