var keterangan1 = "<---Ini adalah Soal Pertama--->";
console.log(keterangan1);
// Game Warewolf
var nama = "John";
var peran = "";
if( nama == "")
{
    console.log("Nama harus diisi!")
}
else if(nama == "John", peran == "")
{
    console.log("Halo John, pilih peranmu untuk memulai game!")
}
else if(nama == "Jane", peran == "Penyihir")
{  console.log("Selamat datang di Dunia Werewolf, Jane")
   console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}
else if(nama == "Junita" , peran == "Guard")
{
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf")
}
else if(nama == "Junaedi", peran == "Warewolf")
{
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
};

var keterangan2 = "<---Ini adalah Soal kedua--->";
console.log(keterangan2);
//Switch Case
var hari = 21; 
var bulan = 1;
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';
var bulan = 1;
switch(bulan) {
    case 1:   { console.log('21 Januari 1945'); break; }
    case 2:   { console.log('21 Februari 1945'); break; }
    case 3:   { console.log('21 Maret 1945'); break; }
    case 4:   { console.log('21 April 1945'); break; }
    case 5:   { console.log('21 Mei 1945'); break; }
    case 6:   { console.log('21 Juni 1945'); break; }
    case 7:   { console.log('21 Juli 1945'); break; }
    case 8:   { console.log('21 Agustus 1945'); break; }
    case 9:   { console.log('21 September 1945'); break; }
    case 10:   { console.log('21 Oktober 1945'); break; }
    case 11:   { console.log('21 November 1945'); break; }
    case 12:   { console.log('21 Desember 1945'); break; }
    default:  { console.log('')}
};