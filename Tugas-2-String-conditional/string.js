var keterangan1 = "<---Ini merupakan Soal pertama--->";
console.log(keterangan1);
//Soal Pertama
var word = "JavaScript "; 
var second = "is "; 
var third = "awesome "; 
var fourth = "and "; 
var fifth = "I "; 
var sixth = "love "; 
var seventh = "it! ";
console.log(word.concat(second, third, fourth, fifth, sixth, seventh));


var keterangan2 = "<---Ini merupakan Soal Kedua--->";
console.log(keterangan2);
//Soal Kedua
var sentence = "I am going to be React Native Developer"; 
var kata1 = sentence[0] ; // I
var kata2 = sentence[2]+sentence[3] ; // am
var kata3 = sentence[5]+sentence[6]+sentence[7] +sentence[8]+sentence[9]+sentence[10] //going  
var kata4 = sentence[11] + sentence[12] // to
var kata5 = sentence[14]+sentence[15];// be
var kata6 = sentence[17]+sentence[15]+sentence[2]+sentence[20]+sentence[11] // React 
var kata7 = sentence[23]+sentence[2]+sentence[11]+sentence[7]+sentence[27] +sentence[15] // Native
var kata8 = sentence[30]+ sentence[15]+sentence[27] +sentence[15]+sentence[34] +sentence[6]+sentence[36] +sentence[37]+sentence[38]; // Developer

console.log('First Word: ' + kata1); 
console.log('Second Word: ' + kata2); 
console.log('Third Word: ' + kata3); 
console.log('Fourth Word: ' + kata4); 
console.log('Fifth Word: ' + kata5); 
console.log('Sixth Word: ' + kata6); 
console.log('Seventh Word: ' + kata7); 
console.log('Eighth Word: ' + kata8);


var keterangan3 = "<---ini merupakan Soal ketiga--->";
console.log(keterangan3);
//Soal Ketiga
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); 
var thirdWord2 = sentence2.substring(15,17);  
var fourthWord2 = sentence2.substring (18, 20); 
var fifthWord2 = sentence2.substring(21, 25);  

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

var keterangan4 = "<---Ini merupakan Soal keempat--->"
console.log(keterangan4);
//Soal Keempat

var sentence3 = 'wow JavaScript is so cool'; 
var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14); 
var thirdWord3 = sentence3.substring(15,17);  
var fourthWord3 = sentence3.substring (18, 20); 
var fifthWord3 = sentence3.substring(21, 25);  

var firstWordLength = exampleFirstWord3.length ;
var secondWord3Length = secondWord3.length;
var thirdWord3Length = thirdWord3.length;
var fourthWord3Length = fourthWord3.length;
var fifthWord3Length = fifthWord3.length; 

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWord3Length); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWord3Length); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWord3Length); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWord3Length); 