console.log("Soal Pertama")

class Animal {
    constructor (name, numLegs, cold_blooded, voice) {
      this._name = name
      this._numLegs = numLegs
      this._cold_blooded = cold_blooded
      this._voice = voice
    }
  
    yell () {
      return this._voice
    }
    jump () {
        return this._jump
    }
}
class sheep extends Animal{
    constructor()
    {
        super(`shaun`,4, false, `mbeek`)
        
    }
}
class Ape extends Animal {
    constructor(){
 super(`kera sakti`, 2, false, `Auooo`)
    }
} 
  class Frog extends Animal {
    constructor () {
      super(`buduk`, 4, true, `krokokkk`)
      this._jump = "hop hop"
    }
  }

  console.log("Release 0")
  console.log("=========")
  var shaun = new sheep()
  console.log(`${shaun._name}`) 
  console.log(`${shaun._numLegs}`) 
  console.log(`${shaun._cold_blooded}`)
  console.log("=========")

  
  console.log("Release 1")
  console.log("=========")
  var sungokong = new Ape()
  console.log(`${sungokong.yell()}`) 
  var kodok = new Frog()
  console.log(`${kodok.jump()}`)
  
  
console.log("Soal Kedua")
class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    let output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();  