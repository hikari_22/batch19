
console.log('<---Soal Pertama--->');
function range(startNum, finishNum)
{
    var rangeArray = [];
    if (startNum > finishNum)
    {
        var rangeLength = startNum -finishNum +1;
        for ( var i=0; i < rangeLength ; i++)
    rangeArray.push(startNum-i)
    }   
    else if( startNum < finishNum)
     {
    var rangeLength = finishNum -startNum + 1;
    for ( var i= 0; i < rangeLength ; i++)
     {
        rangeArray.push(startNum+i)
    }
}
else if(!startNum || !finishNum)
{
    return -1
}
return rangeArray
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log('<---Soal Kedua--->')
function rangeWithstep(startNum, finishNum, step)
{
    var rangeArray = [];
    if (startNum > finishNum)
    {
        var current = startNum;
        for (var i =0 ; current >= finishNum; i++)
        {
            rangeArray.push(current)
            current-= step
        }
    }else if( startNum < finishNum)
        {
            var current = startNum;
            for(var i=0; current<=finishNum ; i++)
            {
             rangeArray.push(current)
             current+=step
            } 
        } else if (!startNum || !finishNum || !step)
        {
            return -1
        }
        return rangeArray
}
console.log(rangeWithstep(1, 10, 2)) ;
console.log(rangeWithstep(11, 23, 3));
console.log(rangeWithstep(5, 2, 1)) ;
console.log(rangeWithstep(29, 2, 4));

console.log('<--- Soal ketiga --->');
function sum(startNum, finishNum, step)
{
    var rangeArray = 0;
    if (startNum > finishNum)
    {    
        for (var i = startNum ; i >= finishNum; i-=step)
        {
           rangeArray+=i
           
            
        }
    }else if( startNum < finishNum)
    { for (var i = startNum ; i <= finishNum; i+=step)
    {
           
         rangeArray+=i
    }
    
    }
    

    else if(startNum)
    {
        return startNum
    }
    else
    {
        return 0
    }
        return rangeArray
}
console.log(sum(1, 10,1)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10,1)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("<---Soal Keempat--->");
function dataHandling (input) {
    for (let i = 0; i < input.length; i++) {
      console.log(`Nomor ID: ${input[i][0]}`)
      console.log(`Nama Lengkap: ${input[i][1]}`)
      console.log(`TTL: ${input[i][2]} ${input[i][3]}`)
      console.log(`Hobi: ${input[i][4]}\n`)
    }
  }
  
  // contoh input
  var input = [
    ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
    ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
    ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
    ['0004', 'Bintang Senjaya', 'Martapura', '6/4/1970', 'Berkebun']
  ]
  
  dataHandling(input)

console.log('<---soal Kelima--->');
function balikString (arrayInput) {
    let arrayLength = arrayInput.length
    let reverseWord = ''
  
    for (let counter = 1; counter <= arrayLength; counter++) {
      reverseWord += arrayInput[arrayLength - counter]
    }
    return reverseWord
  }
  
  // Declare variable

  // Print reverse string
  console.log(balikString('Kasur Rusak'));
  console.log(balikString("SanberCode"));
  console.log(balikString("Haji Ijah"));
  console.log(balikString( "racecar"));
  console.log(balikString("I am Sanbers"))
 
  console.log('<---soal Keenam--->');
  function dataHandling2 (input) {
    let tanggal
    let tanggalSort
    let tanggalJoin
    
    input.splice(1, 1, 'Roman Alamsyah Elsharawy')
    input.splice(2, 1, 'Provinsi Bandar Lampung')
    input.splice(4, 1, 'Pria','SMA Internasional Metro')
    console.log(input)
  
   
    tanggal = input[3].split('/')
  
    
    switch (tanggal[1]) {
      case '01':
        console.log('Januari')
        break
  
      case '02':
        console.log('Februari')
        break
  
      case '03':
        console.log('Maret')
        break
  
      case '04':
        console.log('April')
        break
  
      case '05':
        console.log('Mei')
        break
  
      case '06':
        console.log('Juni')
        break
  
      case '07':
        console.log('Juli')
        break
  
      case '08':
        console.log('Agustus')
        break
  
      case '09':
        console.log('September')
        break
  
      case '10':
        console.log('Oktober')
        break
  
      case '11':
        console.log('November')
        break
  
      case '12':
        console.log('Desember')
        break
  
      default:
        console.log('Salah bulan!')
        break
    }
  
    
    tanggalJoin = tanggal.join('-')
    tanggalSort = tanggal.sort(function (value1, value2) { return value2 - value1 })
    console.log(tanggalSort)
  
    
    console.log(tanggalJoin)
  
    
    console.log(input[1].slice(0, 15))
  }
  
  var input = ['0001', 'Roman Alamsyah ', 'Bandar Lampung', '21/05/1989', 'Membaca']
  dataHandling2(input)
  