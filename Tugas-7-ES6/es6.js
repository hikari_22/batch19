console.log('<---Soal Pertama--->')
 goldenFunction=()=>{
    console.log("this is golden!!")
}
goldenFunction()
console.log('<---Soal Kedua--->')
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function (){
        const fullname = firstName + " " + lastName 
        const William = { fullname}
       console.log(William)
        return 
      }
    }
  }

  newFunction("William", "Imoh").fullName() 

  console.log('<---Soal Ketiga--->')
  let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  };
const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName,  lastName, destination, occupation, spell);

console.log('<---Soal Keempat--->')
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [ west, east ]
console.log(combined);

console.log('<--- Soal Kelima--->')
const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, 
${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
Ut enim ad minim veniam`
console.log(before);

