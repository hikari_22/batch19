import React from "react";
import { View, Text, StyleSheet, Button } from "react-native";

import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5,
    backgroundColor : '#FFD700'
  }
});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Home = ({ navigation }) => (
  <ScreenContainer>
    <Text>Skill Screen</Text>
    
  </ScreenContainer>
);

export const Details = ({ route }) => (
  <ScreenContainer>
    <Text>Details Screen</Text>
    {route.params.name && <Text>{route.params.name}</Text>}
  </ScreenContainer>
);

export const Search = ({ navigation }) => (
  <ScreenContainer>
    <Text>Project Screen</Text>
    
  </ScreenContainer>
);
export const Search1 = ({ navigation }) => (
  <ScreenContainer>
    <Text>Add Screen</Text>
    
  </ScreenContainer>
);

export const Project2 = () => (
  <ScreenContainer>
    <Text>Add Screen</Text>
  </ScreenContainer>
);
export const Add2 = () => (
  <ScreenContainer>
    <Text>Add Screen</Text>
  </ScreenContainer>
);

export const Profile = ({ navigation }) => {
  const { signOut } = React.useContext(AuthContext);

  return (
    <ScreenContainer>
      <Text>Profile Screen</Text>
      <Button title="Tab untuk melihat menu" onPress={() => navigation.toggleDrawer()} />
    </ScreenContainer>
  );
};

export const Splash = () => (
  <ScreenContainer>
    <Text>Memuat...</Text>
  </ScreenContainer>
);

export const SignIn = ({ navigation }) => {
  const { signIn } = React.useContext(AuthContext);

  return (
    <ScreenContainer>
      <Text>Login Screen</Text>
      <Button style = {styles.button} title="Menuju Profile Screen" onPress={() => signIn()} />
    </ScreenContainer>
  );
};

